function renderFoodList(foods) {
    let contentHTML  = ""
    foods.reverse().map(item => {
        let contentTr = `
            <tr>
                <td>${item.maMon}</td>
                <td>${item.tenMon}</td>
                <td>${item.giaMon}</td>
                <td>${item.loaiMon ? "<span class='text-danger'>Mặn</span>" : "<span class='text-info'>Chay</span>"}</td>
                <td>${item.hinhAnh}</td>
                <td><button class="btn btn-danger" onclick= "xoaMonAn(${item.maMon})">Xóa</button>
                <button class="btn btn-info" id="btn-delete" onclick= "suaMonAn(${item.maMon})">Sửa</button></td>
            </tr>
        `
        contentHTML += contentTr
    })
    document.getElementById("tbodyFood").innerHTML = contentHTML
}

function fetchFoodList() {
    batLoading()
    axios({
        url: "https://63be4d39f5cfc0949b54f123.mockapi.io/food",
        method: "GET"
    }).then(function(res) {
        tatLoading()
        const foodList = res.data
        renderFoodList(foodList)
    }).catch(function(err) {
        tatLoading()
        console.log("err", err)
    })

}
fetchFoodList()

function xoaMonAn(id) {
    batLoading()
    axios({
        url: `https://63be4d39f5cfc0949b54f123.mockapi.io/food/${id}`,
        method: "DELETE"
    }).then(function(res) {
        tatLoading()
        // xóa món ăn trên server rồi lấy lại danh sách và render ra màn hihh
        fetchFoodList()
    }).catch(function(err) {
        tatLoading()
        console.log("err", err)
    })
}


function themMonAn() {
    const monAn = layThongTinTuForm()
    let invalid = validate(monAn)
    if(invalid) {
        batLoading()
        axios({
            url: `https://63be4d39f5cfc0949b54f123.mockapi.io/food`,
            method: "POST",
            data: monAn
        }).then(function(err) {
            tatLoading()
            fetchFoodList()
        }).catch(function(err) {
            tatLoading()
            console.log("err", err)
        })
    }
}

function suaMonAn(id) {
    batLoading()
    onDisableBtn()
    axios({
        url: `https://63be4d39f5cfc0949b54f123.mockapi.io/food/${id}`,
        method: "GET"
    }).then(function(res) {
        tatLoading()
        capNhatThongTinLenForm(res.data)
    }).catch(function(err) {
        tatLoading()
        console.log("err", err)
    })
}

function capNhatMon() {
    const monAn = layThongTinTuForm()
    let invalid = validate(monAn)
    if(invalid) {
        offDisableBtn()
        batLoading()
        axios({
            url: `https://63be4d39f5cfc0949b54f123.mockapi.io/food/${monAn.maMon}`,
            method: "PUT",
            data: monAn
        }).then(function() {
            tatLoading()
            fetchFoodList()
        }).catch(function(err) {
            tatLoading()
            console.log("err", err)
        })
    }
}