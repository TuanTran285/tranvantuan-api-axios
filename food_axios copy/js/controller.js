function batLoading() {
  document.getElementById("spinner").style.display = "flex";
}
function tatLoading() {
  document.getElementById("spinner").style.display = "none";
}

function layThongTinTuForm() {
  const _maMon = document.getElementById("maMon").value;
  const _tenMon = document.getElementById("tenMon").value;
  const _giaMon = document.getElementById("giaMon").value
  const _loaiMon = document.getElementById("loaiMon").value == "Mặn" ? true : false
  const _hinhAnh = document.getElementById("hinhAnh").value;

  const monAn = {
    maMon: _maMon,
    tenMon: _tenMon,
    giaMon: _giaMon,
    loaiMon: _loaiMon,
    hinhAnh: _hinhAnh
  }

  return monAn
}

function capNhatThongTinLenForm(food) {
  document.getElementById("maMon").value = food.maMon;
  document.getElementById("tenMon").value = food.tenMon;
  document.getElementById("giaMon").value = food.giaMon;
  document.getElementById("loaiMon").value = food.loaiMon == true ? "Mặn" : "Chay"
  document.getElementById("hinhAnh").value = food.hinhAnh;
}


function validate(monAn) {
  let invalid = true
  // kiem tra maMon
  invalid = invalid & (kiemTraChung(monAn.maMon, "tbMaMon") && kiemTraSo(monAn.maMon, "tbMaMon"))
  // kiểm tra tên món
  invalid = invalid & (kiemTraChung(monAn.tenMon, "tbTenMon") && kiemTraChu(monAn.tenMon, "tbTenMon"))
  // kiểm tra giá món
  invalid = invalid & (kiemTraChung(monAn.giaMon, "tbGiaMon") && kiemTraSo(monAn.giaMon, "tbGiaMon"))
  // kiểm tra hình ảnh
  invalid = invalid & kiemTraChung(monAn.hinhAnh, "tbHinhAnh")
  return invalid
  
}


function onDisableBtn() {
  document.getElementById("btnThemMon").disabled  = true
  document.getElementById("btn-update").disabled = false
}
function offDisableBtn() {
  document.getElementById("btnThemMon").disabled = false
  document.getElementById("btn-update").disabled = true
}