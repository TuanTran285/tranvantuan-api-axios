const BASE_URL = "https://63b2c9a15e490925c521a62a.mockapi.io"

function renderFoodList(foods) {
    // true = mặn
    // false = chay
    var contentHTML = "";

    foods.reverse().forEach((item) => {
        var contentTr = `<tr>
            <td>${item.maMon}</td>
            <td>${item.tenMon}</td>
            <td>${item.giaMon}</td>
            <td>${item.loaiMon ? "<span class='text-primary'>Mặn</span>": "<span class='text-success'>Chay</span>"}</td>
            <td>${convertString(50, item.hinhAnh)}</td>
            <td>
            <button class="btn btn-danger" onclick = "xoaMonAn('${item.maMon}')">Xóa</button>
            <button class="btn btn-warning" onclick = "suaMonAn('${item.maMon})">Sửa</button></td>
        </tr>`
        contentHTML += contentTr
    })
    document.getElementById("tbodyFood").innerHTML = contentHTML
}
// gọi api lấy danh sách món ăn từ sever
function fetchFoodList() {
    batLoading()
    axios({
        url: `${BASE_URL}/food`,
        method: "GET"
    }).then((res) => {
        tatLoading()
        var foodList = res.data
        renderFoodList(foodList)
    })
    .catch((err) => {
        console.log("err", err)
        tatLoading()
    })
}

// khi user load trang
fetchFoodList()


function xoaMonAn(id) {
    batLoading()
    console.log(id)
    axios({
        url: `${BASE_URL}/food/${id}`,
        method: "DELETE"
    }).then((res) => {
        tatLoading()
        console.log(res)
        // gọi lại api lấy danh sách sau khi load trang
       fetchFoodList()
    })
    .catch((err) => {
        tatLoading()
        console.log("err", err)
    })
}
function themMonAn() {
    let monAn = layThongTinTuForm()
    axios({
        url: `${BASE_URL}/food`,
        method: "POST",
        data: monAn,
    }).then(function(res) {
        fetchFoodList()
    }).catch(function(err) { {
        console.log("Thêm món ăn", err)
    }
})

}


function suaMonAn(id) {
    batLoading()
    axios({
        url: `${BASE_URL}/food/${id}`,
        method: "GET",
    }).then(function(res) {
        tatLoading()
        console.log("SuaMonAn", res)
    }).catch(function(err) {
        tatLoading()
        console.log("SuaMonAn", err)
    })
}

function capNhatMonAn() {
    var monAn = layThongTinTuForm()
    axios({
        url: `${BASE_URL}/food/${monAn.maMon}`,
        method: "PUT",
        data: monAn,
    })
}